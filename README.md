rc_page
=======

Displays the current page number without needing to initalise a pagination tag which is great for meta tags such as...

`<title>Web Design News - Page 4 - Company Name</title>`

It only has one parameter, that is the number of entries shown per page.

`{exp:rc_page limit="20"}`